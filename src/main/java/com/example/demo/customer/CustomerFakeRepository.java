package com.example.demo.customer;

import java.util.Arrays;
import java.util.List;


public class CustomerFakeRepository implements CustomerRepo{
    @Override
    public List<Customer> getCustomers(){
        return Arrays.asList(
                new Customer(1L,"张学友","password", "email@gmail.com"),
                new Customer(2L,"刘德华","password22", "email@gmail.com")
        );
    }

}
